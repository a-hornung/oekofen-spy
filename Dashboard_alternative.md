## Alternative dashboard visualization for oekofen-spy
The file 'oekofen-spy_dashboard_alternative.json' offers an alternative Grafana dasboard for oekofen-spy (see https://gitlab.com/p3605/oekofen-spy)

Contributed from: https://gitlab.com/a-hornung/oekofen-spy

### Description

- Variable with pellet costs in EUR per tonne, enables calculation of estimated costs per day and week
- Panels optimized for visualization on mobile screens and PC / Laptop
- Combined visualization of heater burning status in buffer temperatures
- Translation of status tags from English to German (workaround for bug in older Pellematic Touch control requires English language settings, see https://github.com/thannaske/oekofen-json-documentation/issues/3)
- Should be (mostly) compatible with data from Pelletronic Touch V3 and V4.

### Example visualization

![dashboard1](/media/oekofen-spy_dashboard_alternative1.png)
![dashboard2](/media/oekofen-spy_dashboard_alternative2.png)
![dashboard3](/media/oekofen-spy_dashboard_alternative3.png)
![dashboard4](/media/oekofen-spy_dashboard_alternative4.png)

